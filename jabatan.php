<?php include 'header.php'; ?>

        <h1 class="mt-3 mb-3">JABATAN</h1>
        <a href="formJabatan.php" class="btn btn-sm btn-success mb-3">Tambah</a>
        <table class="table">
            <thead class="table-info">
                <tr>

                    <th>Jabatan</th>
                    <th>Aksi</th>

                </tr>
            </thead>
            <tbody>
                
                <?php
                    $sql = 'SELECT * FROM jabatan';

                    $query = mysqli_query($conn, $sql);

                    while ($row = mysqli_fetch_object($query)) {
                ?>
                
                <tr>

                    <td><?php echo $row->jabatan; ?></td>
  
                    <td>
                        <a href="formJabatan.php?id_jabatan=<?php echo $row-> id_jabatan; ?>" class="btn btn-sm btn-warning">Ubah</a>
                        <a href="deleteJabatan.php?id_jabatan=<?php echo $row-> id_jabatan; ?>" class="btn btn-sm btn-danger"
                        onclick = "return confirm('Apakah Anda Yakin Menghapus Data?');">Hapus</a>
                    </td>
                </tr>

                <?php
                    } if (!mysqli_num_rows($query)) {
                        echo '<tr><td colspan="6" class="text-center">Tidak ada data.</td></tr>';
                    }
                ?>

            </tbody>
        </table>

<?php include 'footer.php'; ?>