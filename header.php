<?php include 'connect.php'; ?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIMPEG | PEGAWAI</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg bg-info">
        <div class="container-fluid container">
            <a class="navbar-brand" href="#">SIMPEG</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="btn btn-light">
                  <a class="nav-link active" aria-current="page" href="index.php">PEGAWAI</a>
                  </li>
                  <li class="btn btn-light">
                  <a class="nav-link active" href="jabatan.php">JABATAN</a>
                  </li>
              </ul>
              <a href="formLogin.php" class="btn btn-success">Masuk</a>

              <head>
                <style>
                  body{
                    background-color: #fcfcfc;
                    color: #333333;
                    transition: .5s;
                  }
                  input[type="checkbox"]{
                    position: relative;
                    width: 40px;
                    height: 20px;
                    appearance: none;
                    background-color: #434343;
                    outline: none;
                    border-radius: 10px;
                    transition: .5s ease;
                    cursor: pointer;
                  }
                  input[type="checkbox"]:checked{
                    background-color: #fff;
                  }
                  input[type="checkbox"]::before{
                    content: '';
                    position: absolute;
                    width: 16px;
                    height: 16px;
                    background-color: #d3d3d3;
                    border-radius: 50%;
                    top: 2px;
                    left: 2px;
                    transition: .5s ease;
                  }
                  input[type="checkbox"]:checked::before{
                    transform: translateX(20px);
                  }
                  .dark{
                    background-color: #333333;
                    color: #fcfcfc;
                  }
                </style>
              </head>

              <body>
                <text>Dark or Light</text>
                <input type="checkbox" onclick="ubahMode()">
                <script>
                  function ubahMode(){
                    const ubah = document.body;
                    ubah.classList.toggle("dark");
                  }
                </script>   
              </body>

            </div>
        </div>
    </nav>
    <div class="container">